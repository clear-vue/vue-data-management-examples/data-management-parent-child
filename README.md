# Data Management: Parent-Child

This application illustrates the book search application as a parent-child
managed application. All data management is handled by the upper most parent
component and data is then passed down to child components using `props`.

Any change that a child component requests of the data must be passed up to
the top most parent component and then communicated back down through the
component structure to whomever might need it.

This illustrates some key details of this kind of application structure:

1. All data is managed by one component at the top of the component hierarchy. This keeps the data centralized and its management in one place.
2. Parents give data to the children via `props` only.
3. Children communicate back to the parent via event `$emit` calls only.
4. Events that happen deeper in the hierarchy (like the close even from the `BookDetail` component) must be re-emited back to the parent in order to keep all application logic centralized in one, top-level component.

This kind of application is okay for this small of an application, but if this grew in any kind of complexity or had some more components to manage, things will get overwhelming very quickly.