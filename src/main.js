import Vue from "vue";
import ParentComponent from "./ParentComponent.vue";

Vue.config.productionTip = false;

new Vue({
  render: (h) => h(ParentComponent),
}).$mount("#app");
