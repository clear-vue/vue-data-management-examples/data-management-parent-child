import axios from "axios";

export default {
  async searchForTitle(title) {
    const foundData = await axios.get(
      "https://openlibrary.org/search.json?title=" + title.replaceAll(" ", "+")
    );

    return foundData.data.docs;
  },
  getCoverUrlFor(book, size = "M") {
    return `https://covers.openlibrary.org/b/id/${book.cover_i}-${size}.jpg`;
  },
};
